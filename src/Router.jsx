import React from "react";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import { createBrowserHistory } from 'history';
import { PrivateRoute } from './routes/PrivateRoute';
import Dashboard from "layouts/Dashboard";
import Login from "layouts/Login";

const history = createBrowserHistory();

export default class Router extends React.Component {

    render() {
        return (
            <BrowserRouter history={history}>
                <Switch>
                    <Route exact path="/login" component={Login} history={history} />
                    <PrivateRoute exact to="/" component={Dashboard} history={history} />
                </Switch>
            </BrowserRouter>
        );
    }
}
// {indexRoutes.map((prop, key) => {
//     return <Route to={prop.path} component={prop.component} key={key} />;
// })}