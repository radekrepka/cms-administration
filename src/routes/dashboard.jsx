import Dashboard from "views/demo/Dashboard/Dashboard";
import UserProfile from "views/demo/UserProfile/UserProfile";
import TableList from "views/demo/TableList/TableList";
import Typography from "views/demo/Typography/Typography";
import Icons from "views/demo/Icons/Icons";
import Maps from "views/demo/Maps/Maps";
import Notifications from "views/demo/Notifications/Notifications";
import Upgrade from "views/demo/Upgrade/Upgrade";

import Sections from "views/Sections";
import Articles from "views/Articles";
import Editor from "views/EditorPage";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard
  },
  // {
  //   path: "/user",
  //   name: "User Profile",
  //   icon: "pe-7s-user",
  //   component: UserProfile
  // },
  // {
  //   path: "/table",
  //   name: "Table List",
  //   icon: "pe-7s-note2",
  //   component: TableList
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   icon: "pe-7s-news-paper",
  //   component: Typography
  // },
  // { path: "/icons", name: "Icons", icon: "pe-7s-science", component: Icons },
  // { path: "/maps", name: "Maps", icon: "pe-7s-map-marker", component: Maps },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   icon: "pe-7s-bell",
  //   component: Notifications
  // },
  {
    path: "/articles",
    name: "Články",
    icon: "pe-7s-news-paper",
    component: Articles
  },
  {
    path: "/sections",
    name: "Sekce",
    icon: "pe-7s-menu",
    component: Sections
  },
  {
    path: "/editor",
    name: "Editor",
    icon: "pe-7s-menu",
    component: Editor
  },
  // {
  //   upgrade: true,
  //   path: "/upgrade",
  //   name: "Upgrade to PRO",
  //   icon: "pe-7s-rocket",
  //   component: Upgrade
  // },
  { redirect: true, path: "/", to: "/dashboard", name: "Dashboard" }
];

export default dashboardRoutes;
