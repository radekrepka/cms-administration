import React, { Component } from "react";
import {Grid, Row, Col, Table} from "react-bootstrap";
import Card from "components/Card/Card";
import './Articles.css';
import axios from "axios";
import Swal from 'sweetalert2/dist/sweetalert2.js';
import SweetAlert from 'sweetalert2-react';

export default class Articles extends Component {

    constructor(props) {
        super(props);

        this.state = {
            articles: [],
            frontPage: {},
            isLoading: true
        };

        this.handleDeleteArticle = this.handleDeleteArticle.bind(this);

        this.loadArticles();
    }

    handleDeleteArticle(event) {
        const id = event.target.value;
        if (id) {
            Swal.fire({
                title: 'Opravdu chcete smazat článek?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ano, smazat!',
                cancelButtonText: 'Ne, ponechat.'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Smazáno!',
                        "",
                        'success'
                    );
                    axios.delete(
                        process.env.REACT_APP_API_URL + "articles/" + id)
                        .then(res => {
                            this.loadArticles();
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire(
                        'Zrušeno',
                        "",
                        'error'
                    );
                }
            })
        }
    }

    loadArticles() {
        axios.get(process.env.REACT_APP_API_URL + "articles/without-bodies")
            .then(res => {
                this.setState({ articles: res.data.articles, frontPage: res.data.frontPage, isLoading: false });
            });
    }

    render() {
        const { articles } = this.state;

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col>
                            <Card
                                title="Články"
                                ctTableFullWidth
                                ctTableResponsive
                                category={
                                    <button className="btn btn-success pull-right">
                                        Přidat
                                    </button>
                                }
                                content={
                                    <Table striped hover>
                                        <thead>
                                        <tr>
                                            <th>Obrázek</th>
                                            <th>Titulek</th>
                                            <th>Smazat</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            {articles.map(article =>
                                                <tr>
                                                    <div className="img-container">
                                                        <img alt={article.title} src="//d39-a.sdn.szn.cz/d_39/c_img_G_M/QwIqX.jpeg?fl=cro,0,0,1920,1080%7Cres,600,,1%7Cwebp,75" className="article-img" />
                                                    </div>
                                                    <td className="td-name">{article.title}</td>
                                                    <td>
                                                        <button className="btn btn-danger" onClick={this.handleDeleteArticle} value={article._id}>
                                                            Smazat
                                                        </button>
                                                    </td>
                                                </tr>
                                            )}
                                        </tbody>
                                    </Table>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
