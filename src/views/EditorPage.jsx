import React, { Component } from "react";
import {Grid, Row, Col, Table} from "react-bootstrap";
import 'jodit';
import 'jodit/build/jodit.min.css';
import JoditEditor from "jodit-react";

export default class EditorPage extends Component {

    constructor(props) {
        super(props);

        this.state = {
            content: ""
        };

        this.updateContent = this.updateContent.bind(this);
    }

    updateContent (content) {
        this.setState({ content });


        this.jodit.events.on('afterInsertImage', function (image) {
            console.log(image);
        })
    }

    afterInsertImage(image) {
        console.log(image);
    }

    uploadCallback(file) {
        console.log(file);
        return new Promise(
            (resolve, reject) => {
                const reader = new FileReader();

                reader.onloadend = function() {
                    // Meteor.call('fileStorage.uploadFile',reader.result,file.name,file.type,(err,response)=>{
                    //     console.log(response)
                    //     if(err){
                    //         reject(err)
                    //     }
                    //
                    //     resolve({ data: { link: response.data.url } });
                    // })
                };

                reader.readAsDataURL(file);
            }
        );
    }

    /**
     * @property Jodit jodit instance of native Jodit
     */
    jodit;
    setRef = jodit => this.jodit = jodit;

    config = {
        "uploader": {
            "insertImageAsBase64URI": true,
            "prepareData": function (data) {
                console.log(data);
            },
            "imagesExtensions": [
                "jpg",
                "png",
                "jpeg",
                "gif"
            ]
        }
    };

    render() {
        const { content } = this.state;

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col>
                            <button className="btn btn-success">
                                Publikovat
                            </button>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <JoditEditor
                                editorRef={this.setRef}
                                value={content}
                                config={this.config}
                                onChange={this.updateContent}
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
