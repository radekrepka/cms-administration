import React, { Component } from "react";
import axios from 'axios';
import {Grid, Row, Col, Table} from "react-bootstrap";
import SweetAlert from 'sweetalert2-react';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import Card from "components/Card/Card";

export default class Sections extends Component {

    constructor(props) {
        super(props);

        this.state = {
            sections: [],
            inputSectionNameValue: ''
        };

        this.handleInputSectionNameChange = this.handleInputSectionNameChange.bind(this);
        this.handleAddSection = this.handleAddSection.bind(this);
        this.handleDeleteSection = this.handleDeleteSection.bind(this);
        this.handleChangeSortUp = this.handleChangeSortUp.bind(this);
        this.handleChangeSortDown = this.handleChangeSortDown.bind(this);

        this.loadSections();
    }

    handleInputSectionNameChange(event) {
        this.setState({inputSectionNameValue: event.target.value});
    }

    handleAddSection() {
        const sectionName = this.state.inputSectionNameValue;

        if (sectionName !== ""){
            axios.post(
                process.env.REACT_APP_API_URL + "sections/", {
                    "name": sectionName
                })
                .then(res => {
                    this.loadSections();
                    this.setState({inputSectionNameValue: ""});
                })
                .catch((error) => {
                    console.log(error);
                });
        }
    }

    handleDeleteSection(event) {
        const id = event.target.value;
        if (id) {
            Swal.fire({
                title: 'Opravdu chcete smazat sekci?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ano, smazat!',
                cancelButtonText: 'Ne, ponechat.'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Smazáno!',
                        "",
                        'success'
                    );
                    axios.delete(
                        process.env.REACT_APP_API_URL + "sections/" + id)
                        .then(res => {
                            this.loadSections();
                        })
                        .catch((error) => {
                            console.log(error);
                        });
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire(
                        'Zrušeno',
                        "",
                        'error'
                    );
                }
            })
        }
    }

    handleChangeSortUp(event) {

    }

    handleChangeSortDown(event) {

    }

    loadSections() {
        axios.get(process.env.REACT_APP_API_URL + "sections/without-articles")
            .then(res => {
                this.setState({ sections: res.data.sections});
            });
    }

    render() {
        const { sections } = this.state;

        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col>
                            <Card
                                title="Seznam sekcí"
                                ctTableFullWidth
                                ctTableResponsive
                                content={
                                    <Table striped hover>
                                        <thead>
                                        <tr>
                                            <th>Název</th>
                                            <th>URL</th>
                                            <th>Změnit pořadí</th>
                                            <th>Smazat</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            {sections.map(section =>
                                                <tr>
                                                    <td>{section.name}</td>
                                                    <td>/{section.url}</td>
                                                    <td>
                                                        <button className="btn btn-primary" onClick={this.handleChangeSortUp} value={section._id}>
                                                            <i className="fa fa-sort-up "></i>
                                                        </button>
                                                        <button className="btn btn-primary" onClick={this.handleChangeSortDown} value={section._id}>
                                                            <i className="fa fa-sort-down"></i>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button className="btn btn-danger" onClick={this.handleDeleteSection} value={section._id}>
                                                            Smazat
                                                        </button>
                                                    </td>
                                                </tr>
                                            )}
                                            <tr>
                                                <td>
                                                    <input type="text" placeholder="Název..." className="form-control" value={this.state.inputSectionNameValue} onChange={this.handleInputSectionNameChange} />
                                                </td>
                                                <td>
                                                    <button className="btn btn-success" onClick={this.handleAddSection}>
                                                        Přidat
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}
