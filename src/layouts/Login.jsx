import React, { Component } from "react";

import backgroundImage from "assets/img/background.jpg";
import './Login.css';
import {Redirect, Route} from "react-router-dom";
import axios from "axios";
import NotificationSystem from 'react-notification-system';
import {style} from "variables/Variables.jsx";

export default class Login extends Component {

    constructor(props) {
        super(props);

        if (localStorage.getItem('token')) {
            this.props.history.push('/articles');
        }

        this.state = {
            token: localStorage.getItem('token'),
            _notificationSystem: null,
            redirect: false,
            username: '',
            password: ''
        };

        this.componentDidMount = this.componentDidMount.bind(this);
        this.handleInputUsernameChange = this.handleInputUsernameChange.bind(this);
        this.handleInputPasswordChange = this.handleInputPasswordChange.bind(this);
        this.handleLoginSubmit = this.handleLoginSubmit.bind(this);
    }

    componentDidMount() {
        this.setState({ _notificationSystem: this.refs.notificationSystem });
    }

    handleInputUsernameChange(event) {
        this.setState({username: event.target.value});
    }

    handleInputPasswordChange(event) {
        this.setState({password: event.target.value});
    }

    handleLoginSubmit(event) {
        const { username, password } = this.state;
        
        if (username !== "" && password !== "") {
            axios.post(
                process.env.REACT_APP_API_URL + "users/login",
                {
                    username,
                    password
                }
            )
            .then(res => {
                const newToken = res.data.token;
                localStorage.setItem('token', 'Bearer ' + newToken);
                //this.props.history.push('/articles');
                this.setState({ token: newToken })
                //console.log(res);
            })
            .catch((error) => {
                console.log(error.response);
                if (error.response.status == 401) {
                    this.state._notificationSystem.addNotification({
                        title: (<span data-notify="icon" className="fa fa-warning "></span>),
                        message: (
                            <div>
                                Špatné uživatelské jméno nebo heslo!
                            </div>
                        ),
                        level: 'error',
                        position: 'tr',
                        autoDismiss: 15,
                    });
                } else {
                    this.state._notificationSystem.addNotification({
                        title: (<span data-notify="icon" className="fa fa-warning "></span>),
                        message: (
                            <div>
                                Chyba!
                            </div>
                        ),
                        level: 'error',
                        position: 'tr',
                        autoDismiss: 15,
                    });
                }
            });
        }
        else {
            this.state._notificationSystem.addNotification({
                title: (<span data-notify="icon" className="fa fa-warning "></span>),
                message: (
                    <div>
                        Vyplňte přihlašovací jméno a heslo!
                    </div>
                ),
                level: 'error',
                position: 'tr',
                autoDismiss: 15,
            });
        }

        event.preventDefault();
    }

    render() {
        const { token } = this.state;

        if (token) {
            return <Redirect
                to={{
                    pathname: "/"
                }}
            />
        }

        return (
            <div className="full-page login-page" data-color="black" data-image={backgroundImage}>
                <NotificationSystem ref="notificationSystem" style={style} />
                <div className="content">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                                <form onSubmit={this.handleLoginSubmit}>
                                    <div className="card">
                                        <div className="header text-center">
                                            <h4 className="title">Login</h4>
                                            <p className="category"></p>
                                        </div>
                                        <div className="content">
                                            <div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        Username
                                                    </label>
                                                    <input type="text" placeholder="Username" className="form-control" value={this.state.username} onChange={this.handleInputUsernameChange} />
                                                </div>
                                                <div className="form-group">
                                                    <label className="control-label">
                                                        Password
                                                    </label>
                                                    <input type="password" placeholder="Password" className="form-control" value={this.state.password} onChange={this.handleInputPasswordChange} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="footer text-center">
                                            <div className="legend">
                                                <button type="submit" className="btn-fill btn-wd btn btn-info">
                                                    Login
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="full-page-background" style={{ backgroundImage: "url(" + backgroundImage + ")" }}></div>
            </div>
        );
    }
}
