import React from "react";
import ReactDOM from "react-dom";
import axios from 'axios';
import Router from 'Router'

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/css/animate.min.css";
import "./assets/sass/light-bootstrap-dashboard.css?v=1.2.0";
import "./assets/css/demo.css";
import "./assets/css/pe-icon-7-stroke.css";

//import Swal from 'sweetalert2/dist/sweetalert2.js'
import 'sweetalert2/src/sweetalert2.scss'

const token = localStorage.getItem('token');

axios.interceptors.request.use(function (config) {
    config.headers.Authorization = token;
    return config;
});

ReactDOM.render(
  <Router/>,
  document.getElementById("root")
);
